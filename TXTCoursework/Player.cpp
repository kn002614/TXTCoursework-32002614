#include "Player.h"
#include "Item.h"
#include "Inventory.h"

Player::Player(const std::string& name, int health) : Character(name, health), location(nullptr) {}

Room* Player::GetLocation() const {
    return location;
}

Room* Player::SetLocation(Room* newLocation) {
    location = newLocation;
    return newLocation;
}

void Player::Move() {
    // Implement movement logic
    std::cout << "Player moves." << "\n";
}
