#include "Area.h"
#include "Room.h" 
#include <fstream> 
#include <iostream> 
#include <sstream>

void Area::AddRoom(const std::string& name, Room* room) {
    rooms[name] = room;
}

Room* Area::GetRoom(const std::string& name) {
    auto it = rooms.find(name);
    if (it != rooms.end()) {
        return it->second;
    }
    else {
        return nullptr;
    }
}

void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);

    if (room1 && room2) {
        room1->AddExit(direction, room2);
        room2->AddExit(Area::GetOppositeDirection(direction), room1);
    }
    else {
        std::cerr << "Error: One or both rooms not found!" << "\n";
    }
}

void Area::LoadMapFromFile(const std::string& filename) {
    std::ifstream file(filename);
    std::string line;

    if (file.is_open()) {
        while (std::getline(file, line)) {
            std::stringstream ss(line);
            std::string roomName, description;
            ss >> roomName;
            std::getline(ss, description);

            Room* room = new Room(description);
            AddRoom(roomName, room);

            std::string exitDirection, connectedRoomName;
            while (ss >> exitDirection >> connectedRoomName) {
                ConnectRooms(roomName, connectedRoomName, exitDirection);
            }
        }
        file.close();
    }
    else {
        std::cerr << "Error: File " << filename << " not found.\n";
    }
}

std::string Area::GetOppositeDirection(const std::string& direction) {
    if (direction == "north") return "south";
    else if (direction == "south") return "north";
    else if (direction == "east") return "west";
    else if (direction == "west") return "east";
    // Add more directions if needed
    else return ""; // Invalid direction
}

