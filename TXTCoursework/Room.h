#pragma once
#ifndef ROOM_H
#define ROOM_H

#include <string>
#include <map>
#include <vector>
#include "Item.h"

class Room {
private:
    std::string description;
    std::map<std::string, Room*> exits;
    std::vector<Item> items;

public:
    Room(const std::string& description);

    void AddItem(const Item& item);

    void RemoveItem(const Item& item);

    void AddExit(const std::string& direction, Room* room);

    std::string GetDescription() const;

    const std::vector<Item>& GetItems() const;

    Room* GetExit(const std::string& direction) const;

    void PrintExits() const;
};

#endif // ROOM_H
