#pragma once
#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "Character.h"
#include "Room.h"
#include "inventory.h"

class Player : public Character {
private:
    Room* location;
    Inventory inventory;
public:
    Player(const std::string& name, int health);

    Room* GetLocation() const;

    Room* SetLocation(Room* newLocation);

    void Move();

    Inventory& getInventory() {
        return inventory;
    }

    void addItem(const Item& item);
};
#endif // PLAYER_H
