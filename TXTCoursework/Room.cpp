#include "Room.h"

Room::Room(const std::string& description) : description(description) {}

void Room::AddItem(const Item& item) {
    items.push_back(item);
}

void Room::RemoveItem(const Item& item) {
    auto it = find(items.begin(), items.end(), item);
    if (it != items.end()) {
        items.erase(it);
    }
}

void Room::AddExit(const std::string& direction, Room* room) {
    exits[direction] = room;
}

std::string Room::GetDescription() const {
    return description;
}

const std::vector<Item>& Room::GetItems() const {
    return items;
}

Room* Room::GetExit(const std::string& direction) const {
    auto it = exits.find(direction);
    if (it != exits.end()) {
        return it->second;
    }
    else {
        return nullptr;
    }
}

void Room::PrintExits() const {
    std::cout << "Available exits: ";
    for (const auto& exit : exits) {
        std::cout << exit.first << " ";
    }
    std::cout << "\n";
}
