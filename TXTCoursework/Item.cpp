#include "Item.h"
#include "Player.h"

Item::Item(const std::string& name, const std::string& description) : name(name), description(description) {}

void Item::Interact() const {
    std::cout << "You picked up " << name << ".\n";
}

std::string Item::GetName() const {
    return name;
}

std::string Item::GetDescription() const {
    return description;
}