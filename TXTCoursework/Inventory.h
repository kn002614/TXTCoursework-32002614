#ifndef INVENTORY_H
#define INVENTORY_H

#include "Item.h" // Include the item.h header file
#include <map>
#include <string>

class Inventory {
private:
    std::map<Item, int> items; // Map to store items and their quantities

public:
    // Add item to inventory
    void addItem(const std::string& name, const std::string& description, int quantity);

    // Remove item from inventory
    void removeItem(const std::string& name, int quantity);

    // Display inventory
    void displayInventory();

    std::map<Item, int> getItems() const {
        return items;
    }
};

#endif // INVENTORY_H
