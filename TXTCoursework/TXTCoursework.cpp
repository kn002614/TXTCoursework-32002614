#include <iostream>
#include <string>
#include "Area.h"
#include "Room.h"
#include "Item.h"
#include "Player.h"
#include "Inventory.h"

int main() {
    Area dungeon;
    dungeon.LoadMapFromFile("dungeon.txt");

    Inventory inventory;
    
    // Create Rooms
    Room startRoom("You are in a dimly lit room.");
    Room hallway("You are in a long hallway.");
    Room treasureRoom("You have entered a treasure room!");

    // Define exits between rooms
    startRoom.AddExit("north", &hallway);
    hallway.AddExit("south", &startRoom);
    hallway.AddExit("north", &treasureRoom);
    treasureRoom.AddExit("south", &hallway);

    // Create Items
    Item key("Key", "A shiny key that looks important.");
    Item sword("Sword", "A sharp sword with a golden hilt.");
    Item apple("Apple", "A nutritious red apple.");

    // Add items to rooms
    startRoom.AddItem(key);
    treasureRoom.AddItem(sword);

    // Create a Player
    Player player("Alice", 100);

    // Add items to the players inventory
    player.getInventory().addItem(apple.GetName(), apple.GetDescription(), 1);

    // Set the player's starting location
    player.SetLocation(&startRoom);

    // Game loop (basic interaction)
    while (true) {
        Room* currentRoom = player.GetLocation();
        std::cout << "Current Location: " << currentRoom->GetDescription() << "\n";
        std::cout << "Items in the room:" << "\n";
        for (const Item& item : player.GetLocation()->GetItems()) {
            std::cout << "- " << item.GetName() << ": " <<
                item.GetDescription() << "\n";
        }
        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. View inventory |";
        std::cout << "5. Quit" << "\n";
        int choice;
        std::cin >> choice;
        if (choice == 1) {
            // Player looks around (no action required)
            std::cout << "You look around the room." << "\n";
        }
        else if (choice == 2) {
            // Player interacts with an item in the room
            std::cout << "Enter the name of the item you want to interact with:";
            std::string itemName;
            std::cin >> itemName;
            for (const Item& item : player.GetLocation()->GetItems()) {
                if (item.GetName() == itemName) {
                    player.getInventory().addItem(item.GetName(), item.GetDescription(), 1);
                    currentRoom->RemoveItem(item);
                    item.Interact();
                    break;
                }
            }
        }
        else if (choice == 3) {
            // Player moves to another room
            std::cout << "Enter the direction (e.g., north, south): ";
            std::string direction;
            std::cin >> direction;
            Room* nextRoom = player.GetLocation()->GetExit(direction);
            if (nextRoom != nullptr) {
                player.SetLocation(nextRoom);
                std::cout << "You move to the next room." << "\n";
            }
            else {
                std::cout << "You can't go that way." << "\n";
            }
        }
        else if (choice == 4) {
            player.getInventory().displayInventory();
            std::string selectedItem;
            std::cin >> selectedItem;
            for (const auto& pair : player.getInventory().getItems()) {
                if (pair.first.GetName() == selectedItem) {
                    std::cout << "Selected item: " << pair.first.GetName() << "\n";
                    std::cout << "Description: " << pair.first.GetDescription() << "\n";
                    std::cout << "Drop selected item? 1. Yes| 2. No: \n";
                    int drop;
                    std::cin >> drop;
                    if (drop == 1) {
                        player.getInventory().removeItem(pair.first.GetName(), 1);
                        currentRoom->AddItem(pair.first);
                    }
                }
                else
                {
                    std::cout << "Item not found in inventory." << "\n";
                }
            }
        }
        else if (choice == 5) {
            // Quit the game
            std::cout << "Goodbye!" << "\n";
            break;
        }
        else {
            std::cout << "Invalid choice. Try again." << "\n";
        }
    }
    return 0;
}
