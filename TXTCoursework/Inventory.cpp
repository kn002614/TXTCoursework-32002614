#include <iostream>
#include <map>
#include <string>
#include "Item.h"
#include "Inventory.h"

// Declare items as a global variable
std::map<Item, int> items;

// Add item to inventory
void Inventory::addItem(const std::string& name, const std::string& description, int quantity) {
    Item item(name, description); // Create Item object
    items[item] += quantity; // If item already exists, add quantity to it; otherwise, create a new entry
    std::cout << "Added " << quantity << " " << name << "(s) to inventory." << "\n";
}

// Remove item from inventory
void Inventory::removeItem(const std::string& name, int quantity) {
    Item item(name, ""); // Create Item object with empty description
    if (items.find(item) != items.end()) {
        if (items[item] >= quantity) {
            items[item] -= quantity;
            std::cout << "Removed " << quantity << " " << name << "(s) from inventory." << "\n";
        }
        else {
            std::cout << "Not enough " << name << " in inventory." << "\n";
        }
    }
    else {
        std::cout << "Item " << name << " not found in inventory." << "\n";
    }
}

// Display inventory
void Inventory::displayInventory() {
    std::cout << "Select item in inventory: \n";
    for (const auto& pair : items) {
        std::cout << pair.first.GetName() << ": " << pair.second << "\n";
    }
}
