#ifndef ITEM_H
#define ITEM_H

#include <string>
#include <iostream>

class Item {
private:
    std::string name;
    std::string description;

public:
    Item(const std::string& name, const std::string& description);

    void Interact() const;

    std::string GetName() const;

    std::string GetDescription() const;

    bool operator<(const Item& other) const {
        return name < other.name;
    }

    bool operator==(const Item& other) const {
        return name == other.name && description == other.description;
    }
};



#endif // ITEM_H
